//
//  CellTemplate.swift
//  ylee-test
//
//  Created by Савелий Вепрев on 11/07/2019.
//  Copyright © 2019 Савелий Вепрев. All rights reserved.
//

import UIKit
import Foundation


class CatCell: AppCell {
    var category: String? {
        didSet {
            if let name = category {
                nameLabel.text = name
            }
        }
    }
    override func setupViews() {
        backgroundColor = UIColor.gray
        addSubview(nameLabel)
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.heightAnchor.constraint(equalToConstant: 60).isActive = true
        nameLabel.widthAnchor.constraint(equalToConstant: frame.width).isActive = true
        nameLabel.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
    }
}
class AppCell: UICollectionViewCell {
    var template: UIImage? {
        didSet {
            if let image = template {
                imageView.image = image
            }
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    let imageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "minimal1")
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    let nameLabel: UILabel = {
        let label = UILabel()
        label.text = "MINIMAL"
        label.font = UIFont.systemFont(ofSize: 14)
        label.numberOfLines = 1
        label.textAlignment = .center
        label.textColor = UIColor.white
        return label
    }()
    func setupViews() {
        addSubview(imageView)        
        imageView.frame = CGRect(x:0, y: 0, width: frame.width, height: frame.height)
    }
}

