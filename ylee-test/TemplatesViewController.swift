//
//  ViewController.swift
//  ylee-test
//
//  Created by Савелий Вепрев on 11/07/2019.
//  Copyright © 2019 Савелий Вепрев. All rights reserved.
//

import UIKit


struct Pack {
    let nameCategory: String!
    let imageTemplates:  [UIImage?]
}
class TemplatesViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    private let cellId = "cellId"
    private let cellCategoryId = "cellCategoryId"
    let packes = [
        Pack(nameCategory: "Первая", imageTemplates: [UIImage(named: "minimal1"), UIImage(named: "minimal1"), UIImage(named: "minimal1"), UIImage(named: "minimal1")] ),
        Pack(nameCategory: "Вторая", imageTemplates: [UIImage(named: "minimal2"), UIImage(named: "minimal2"), UIImage(named: "minimal2"), UIImage(named: "minimal2")] ),
        Pack(nameCategory: "Третья", imageTemplates: [UIImage(named: "minimal1"), UIImage(named: "minimal1"), UIImage(named: "minimal1"), UIImage(named: "minimal1")] ),
        Pack(nameCategory: "Четвертая", imageTemplates: [UIImage(named: "minimal2"), UIImage(named: "minimal2"), UIImage(named: "minimal2"), UIImage(named: "minimal2")] ),
        Pack(nameCategory: "Пятая", imageTemplates: [UIImage(named: "minimal1"), UIImage(named: "minimal1"), UIImage(named: "minimal1"), UIImage(named: "minimal1")] ),
    ]
    
    var lastTemplate = 0
    var currentTemplate = 0
    
    let templatesCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        
        collectionView.backgroundColor = UIColor.clear
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.alwaysBounceHorizontal = true
        return collectionView
    }()
    let categoryCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        
        collectionView.backgroundColor = UIColor.clear
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.alwaysBounceHorizontal = true
        return collectionView
    }()
    let nameScreen: UILabel = {
        let label = UILabel()
        label.text = "Templates"
        label.font = UIFont(name: "Verdana-Bold", size: 24)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor(red:0.21, green:0.21, blue:0.21, alpha:1.0)
        return label
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        setupViews()
    }
    func setupViews(){
        self.view.backgroundColor = UIColor.white
        templatesCollectionView.dataSource = self
        templatesCollectionView.delegate = self
        
        categoryCollectionView.dataSource = self
        categoryCollectionView.delegate = self
        
        templatesCollectionView.register(AppCell.self, forCellWithReuseIdentifier: cellId)
        categoryCollectionView.register(CatCell.self, forCellWithReuseIdentifier: cellCategoryId)
        
        view.addSubview(nameScreen)
        view.addSubview(templatesCollectionView)
        view.addSubview(categoryCollectionView)
        
        nameScreen.topAnchor.constraint(equalTo: view.topAnchor, constant: 40).isActive = true
        nameScreen.heightAnchor.constraint(equalToConstant: 50).isActive = true
        nameScreen.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16).isActive = true
        nameScreen.widthAnchor.constraint(equalToConstant: view.frame.width).isActive = true
        
        templatesCollectionView.topAnchor.constraint(equalTo: nameScreen.bottomAnchor, constant: 0).isActive = true
        templatesCollectionView.heightAnchor.constraint(equalToConstant: view.frame.height).isActive = true
        templatesCollectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16).isActive = true
        templatesCollectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16).isActive = true
        
        categoryCollectionView.topAnchor.constraint(equalTo: templatesCollectionView.bottomAnchor, constant: 16).isActive = true
        categoryCollectionView.heightAnchor.constraint(equalToConstant: 60).isActive = true
        categoryCollectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16).isActive = true
        categoryCollectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16).isActive = true
        categoryCollectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -16).isActive = true
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.templatesCollectionView{
            return packes[currentTemplate].imageTemplates.count
        }
        else{
           return packes.count
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.templatesCollectionView{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! AppCell
            cell.imageView.image = self.packes[currentTemplate].imageTemplates[indexPath.row]
            return cell
        }
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellCategoryId, for: indexPath) as! CatCell
            cell.nameLabel.text =  packes[indexPath.row].nameCategory
            if(currentTemplate == indexPath.row){
                cell.backgroundColor = UIColor.black
            }
            else {
                cell.backgroundColor = UIColor.gray
            }
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == self.templatesCollectionView{
            return CGSize(width: view.frame.width/1.2, height: view.frame.height)
        }
        else{
            return CGSize(width:100, height: collectionView.frame.height)
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if lastTemplate != indexPath.row && collectionView == self.categoryCollectionView {
            currentTemplate = indexPath.row
            lastTemplate = indexPath.row
            self.templatesCollectionView.reloadData()
            self.categoryCollectionView.reloadData()
            scrollToDesired(collectionView, .centeredHorizontally, currentTemplate)
            scrollToDesired(templatesCollectionView, .left, 0)
        }
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let contentOffsetX = scrollView.contentOffset.x
        if scrollView == self.templatesCollectionView {
            if lastTemplate != packes.count - 1 && contentOffsetX >= (scrollView.contentSize.width - scrollView.bounds.width) + 60 {
                currentTemplate = currentTemplate == packes.count ? packes.count : currentTemplate + 1
                scrollHorizontalToPack()
            } else if lastTemplate != 0 && contentOffsetX <= -60 {
                currentTemplate = currentTemplate == 0 ? 0 : currentTemplate - 1
                scrollHorizontalToPack()
            }
        }
    }
    func scrollHorizontalToPack() {
        lastTemplate = currentTemplate
        self.templatesCollectionView.reloadData()
        self.categoryCollectionView.reloadData()
        scrollToDesired(templatesCollectionView, .left, 0)
        scrollToDesired(categoryCollectionView, .centeredHorizontally, currentTemplate )
    }
    private func scrollToDesired(_ collectionView: UICollectionView, _ position: UICollectionView.ScrollPosition, _ indexOf: Int){
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500), execute: {
            collectionView.scrollToItem(at: IndexPath(item: indexOf, section: 0), at: position, animated: true)
        })
    }
}
